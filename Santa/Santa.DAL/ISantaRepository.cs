using Santa.BL.Models;

namespace Santa.DAL;

public interface ISantaRepository
{
    Task InsertGroup(
        Group group,
        CancellationToken ct);

    Task<IReadOnlyDictionary<long, Group>> GetGroups(
        CancellationToken ct);

    Task ChangeGroup(
        long id,
        Group @group,
        CancellationToken ct);

    Task DeleteGroup(long id, CancellationToken ct);

    Task<long> AddParticipant(
        Participant participant,
        CancellationToken ct);
}