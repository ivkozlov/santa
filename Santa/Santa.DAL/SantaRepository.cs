﻿using System.Data;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;
using Santa.BL.Models;

namespace Santa.DAL;

public sealed class SantaRepository : ISantaRepository
{
    private readonly IDbConnection _connection;

    public SantaRepository(IConfiguration configuration)
    {
        _connection = new NpgsqlConnection(configuration.GetConnectionString("GroupDb"));;
    }

    public async Task InsertGroup(Group @group, CancellationToken ct)
    {
        var query = "INSERT INTO santa_group (name, description) VALUES (@Name, @Description)";
        await _connection.ExecuteAsync(
            query,
            new
            {
                Name = group.Name,
                Description = group.Description
            });
    }

    public async Task<IReadOnlyDictionary<long, Group>> GetGroups(CancellationToken ct)
    {
        try
        {
            var query = "SELECT id, name, description FROM santa_group";
            var groups = await _connection.QueryAsync< (long Id, string Name, string Description)>(
                query);

            var result = groups
                .ToDictionary(
                    tuple => tuple.Id,
                    tuple => new Group(
                        tuple.Name,
                        tuple.Description,
                        new HashSet<Participant>()));

            return result;
        }
        catch
        {
            return new Dictionary<long, Group>();
        }
    }

    public async Task ChangeGroup(long id, Group @group, CancellationToken ct)
    {
        var query = "UPDATE santa_group SET name = @Name, description = @Description WHERE id = @Id";
        await _connection.ExecuteAsync(
            query,
            new
            {
                Id = id,
                Name = group.Name,
                Description = group.Description
            });
    }

    public async Task DeleteGroup(long id, CancellationToken ct)
    {
        var query = "DELETE FROM santa_group WHERE id = @Id";
        await _connection.ExecuteAsync(
            query,
            new
            {
                Id = id,
            });
    }

    public async Task<long> AddParticipant(Participant participant, CancellationToken ct)
    {
        var query = "INSERT INTO participant (name, wish, group_id) VALUES (@Name, @Wish, @GroupId) RETURNING id";

        var res = await _connection.QuerySingleAsync<long>(
            query,
            new
            {
                participant.Name,
                participant.Wish,
                participant.GroupId
            });

        return res;
    }
}