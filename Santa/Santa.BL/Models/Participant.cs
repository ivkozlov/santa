namespace Santa.BL.Models;

public sealed record Participant(
    string Name,
    string Wish,
    long GroupId,
    Participant Recipient);