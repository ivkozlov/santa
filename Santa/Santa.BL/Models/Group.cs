namespace Santa.BL.Models;

public sealed record Group(
    string Name,
    string Description,
    IReadOnlySet<Participant> Participants);