using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace Santa.Contracts;

public class ParticipantDto
{
    [Required(AllowEmptyStrings = false)]
    public string Name { get; set; }

    [AllowNull]
    public string Wish { get; set; } = null;
}