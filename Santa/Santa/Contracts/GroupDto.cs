using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace Santa.Contracts;

public sealed record GroupDto
{
    [Required(AllowEmptyStrings = false)]
    public string Name { get; set; }

    [AllowNull] public string Description { get; set; } = "";
}