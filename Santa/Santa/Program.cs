using Santa.DAL;
using Santa.Services;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddScoped<ISantaRepository, SantaRepository>();
builder.Services.AddScoped<ISantaService, SantaService>();
builder.Services.AddControllers();

var app = builder.Build();

app.UseRouting();
app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers(); // Map attribute-routed API controllers
});
app.MapGet("/", () => "Hello World!");

app.Run();