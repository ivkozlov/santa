using Microsoft.AspNetCore.Mvc;
using Santa.BL.Models;
using Santa.Contracts;
using Santa.Services;

namespace Santa.Controllers;

[ApiController]
public sealed class SantaController : ControllerBase
{
    private readonly ISantaService _santaService;

    public SantaController(ISantaService santaService)
    {
        _santaService = santaService;
    }

    [HttpGet("/groups")]
    public async Task<IActionResult> GetGroups(CancellationToken ct)
    {
        var groups = await _santaService.GetGroups(ct);

        var res = groups
            .Select(
                group => new
                {
                    Id = group.Key,
                    group.Value.Name,
                    group.Value.Description
                })
            .ToArray();

        return Ok(res);
    }

    [HttpPost("/group")]
    public async Task<IActionResult> AddGroup(
        GroupDto group,
        CancellationToken ct)
    {
        var model = new Group(
            group.Name,
            group.Description,
            new HashSet<Participant>());

        await _santaService.InsertGroup(model, ct);

        return Ok();
    }

    [HttpPut("/group/{id}")]
    public async Task<IActionResult> ChangeGroup(
        [FromRoute] long id,
        [FromBody] GroupDto group,
        CancellationToken ct)
    {
        var model = new Group(
            group.Name,
            group.Description,
            new HashSet<Participant>());

        await _santaService.ChangeGroup(id, model, ct);

        return Ok();
    }

    [HttpDelete("/group/{id}")]
    public async Task<IActionResult> DeleteGroup(
        [FromRoute] long id,
        CancellationToken ct)
    {
        await _santaService.DeleteGroup(id, ct);

        return Ok();
    }

    [HttpPost("group/{groupId}/participant")]
    public async Task<IActionResult> AddParticipant(
        [FromRoute] long groupId,
        [FromBody] ParticipantDto participantDto,
        CancellationToken ct)
    {
        var model = new Participant(
            participantDto.Name,
            participantDto.Wish,
            groupId,
            null);

        var id = await _santaService.AddParticipant(model, ct);

        return Ok(id);
    }
}