using Santa.BL.Models;
using Santa.DAL;

namespace Santa.Services;

public sealed class SantaService : ISantaService
{
    private readonly ISantaRepository _santaRepository;

    public SantaService(ISantaRepository santaRepository)
    {
        _santaRepository = santaRepository;
    }

    public async Task<IReadOnlyDictionary<long, Group>> GetGroups(CancellationToken ct)
    {
        var result = await _santaRepository.GetGroups(ct);

        return result;
    }

    public async Task InsertGroup(Group @group, CancellationToken ct)
    {
        await _santaRepository.InsertGroup(group, ct);
    }

    public async Task ChangeGroup(long id, Group @group, CancellationToken ct)
    {
        await _santaRepository.ChangeGroup(id, group, ct);
    }

    public async Task DeleteGroup(long id, CancellationToken ct)
    {
        await _santaRepository.DeleteGroup(id, ct);
    }

    public async Task<long> AddParticipant(Participant participant, CancellationToken ct)
    {
        var id = await _santaRepository.AddParticipant(participant, ct);

        return id;
    }
}