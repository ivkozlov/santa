using Santa.BL.Models;

namespace Santa.Services;

public interface ISantaService
{
    Task<IReadOnlyDictionary<long, Group>> GetGroups(CancellationToken ct);

    Task InsertGroup(Group group, CancellationToken ct);

    Task ChangeGroup(
        long id,
        Group @group,
        CancellationToken ct);
    
    Task DeleteGroup(long id, CancellationToken ct);

    Task<long> AddParticipant(Participant participant, CancellationToken ct);
}